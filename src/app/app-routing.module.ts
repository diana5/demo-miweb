import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {ServiciosComponent} from './servicios/servicios.component';
import {ContactoComponent} from './contacto/contacto.component';
import {OfertasComponent} from './ofertas/ofertas.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';

import {MatSelectModule} from '@angular/material/select';





import {HomeComponent} from './home/home.component';

const routes: Routes=[
  {path:'', redirectTo:'/home', pathMatch:'full'},
  {path:'home', component: HomeComponent},
  {path:'servicios', component: ServiciosComponent},
  {path:'ofertas', component: OfertasComponent},
  {path:'contacto', component: ContactoComponent}



  
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    MatExpansionModule,
     MatSelectModule,
     MatFormFieldModule
    
  ],
  exports:[RouterModule]
})
export class AppRoutingModule { }
